﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Reflection;

namespace PMS.Host
{
    /// <summary>
    /// Entry point class
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Entry point with arguments
        /// </summary>
        /// <param name="args">The application arguments.</param>
        public static void Main(string[] args)
        {
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .ConfigureLogging((builderContext, logBuilder) =>
                {
                    logBuilder.AddConfiguration(builderContext.Configuration.GetSection("Logging"));
                    if (IsDebug())
                    {
                        logBuilder.AddConsole();
                    }
                })
                .Build()
                .Run();
        }

        private static bool IsDebug() =>
            Assembly
                .GetEntryAssembly()
                .GetCustomAttribute<DebuggableAttribute>()?
                .IsJITTrackingEnabled ?? 
            false;
    }
}
