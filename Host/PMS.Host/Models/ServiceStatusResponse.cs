﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Runtime.Serialization;

namespace PMS.Host.Models
{
    /// <summary>
    /// Describes service status response items
    /// </summary>
    [DataContract]
    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class ServiceStatusResponse
    {
        /// <summary>
        /// Gets or sets the product version.
        /// </summary>
        [DataMember(Name = "version")]
        public Version Version { get; set; }

        /// <summary>
        /// Gets or sets the environment when running service.
        /// </summary>
        [DataMember(Name = "environment")]
        public JObject Environment { get; set; }

        /// <summary>
        /// Gets or sets the machine information.
        /// </summary>
        [DataMember(Name = "machineInfo")]
        public JObject MachineInfo { get; set; }
    }
}