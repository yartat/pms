using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace PMS.Host.Models
{
    /// <summary>
    /// Describes project members
    /// </summary>
    [DataContract]
    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Project
    {
        /// <summary>
        /// Gets or sets the project code.
        /// </summary>
        [DataMember(Name = "code")]
        [Required]
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the parent project code reference.
        /// </summary>
        [DataMember(Name = "parent")]
        public string ParentProject { get; set; }

        /// <summary>
        /// Gets or sets the project name.
        /// </summary>
        [DataMember(Name = "name")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the project start date.
        /// </summary>
        [DataMember(Name = "startDate")]
        [Required]
        public DateTimeOffset StartDate { get; set; }

        /// <summary>
        /// Gets or sets the project expiration date.
        /// </summary>
        [DataMember(Name = "finishDate")]
        public DateTimeOffset? FinishDate { get; set; }

        /// <summary>
        /// Gets or sets the project state.
        /// </summary>
        [DataMember(Name = "state")]
        [Required]
        public ProjectState State { get; set; }
    }
}