﻿namespace PMS.Host.Models
{
    /// <summary>
    /// Describes possible task states
    /// </summary>
    public enum TaskState
    {
        /// <summary>
        /// The task planned state
        /// </summary>
        Planned,

        /// <summary>
        /// The task in progress state
        /// </summary>
        InProgress,

        /// <summary>
        /// The task completed state
        /// </summary>
        Completed
    }
}
