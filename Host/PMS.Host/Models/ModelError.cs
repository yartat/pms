﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using PMS.Host.Exceptions;
using System;

namespace PMS.Host.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ModelError
    {
        private readonly ILogger _logger;
        private readonly ModelStateDictionary _errors = new ModelStateDictionary();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        public ModelError(ILogger logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Check value for empty value and add it to errors list if empty
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public ModelError AddIfEmpty(string fieldName, string value) =>
            AddError(fieldName, $"{fieldName} is missing", () => string.IsNullOrEmpty(value));

        /// <summary>
        /// Check value for empty value and add it to errors list if empty
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public ModelError AddIfNull<T>(string fieldName, T value)
            where T : class =>
            AddError(fieldName, $"{fieldName} is null", () => value == null);

        /// <summary>
        /// Check value for empty value and add it to errors list if empty
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="isWrong"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public ModelError AddIfWrong(string fieldName, bool isWrong, string message = null) =>
            AddError(fieldName,
                string.IsNullOrEmpty(message)
                    ? $"{fieldName} contains an invalid value"
                    : message,
                () => isWrong);

        private ModelError AddError(string fieldName, string message, Func<bool> checkFunc)
        {
            if (checkFunc == null)
                throw new ArgumentNullException(nameof(checkFunc));
            if (!checkFunc())
                return this;

            _logger?.LogDebug(message);
            _errors.AddModelError(fieldName, message);
            return this;
        }

        /// <summary>
        /// Propogate exception if errors is exists
        /// </summary>
        /// <exception cref="ModelErrorExceptions"></exception>
        public void ThrowIfAny()
        {
            if (_errors.Any())
                throw new ModelErrorExceptions(_errors);
        }

        /// <summary>
        /// Get errors 
        /// </summary>
        /// <returns>errors if any exists or null</returns>
        /// <see cref="ModelStateDictionary"/>
        public ModelStateDictionary GetErrors() => _errors.Any() ? _errors : null;
    }
}