﻿namespace PMS.Host.Models
{
    /// <summary>
    /// Describes possible project states
    /// </summary>
    public enum ProjectState
    {
        /// <summary>
        /// The project planned state
        /// </summary>
        Planned,

        /// <summary>
        /// The project in progress state
        /// </summary>
        InProgress,

        /// <summary>
        /// The project completed state
        /// </summary>
        Completed
    }
}
