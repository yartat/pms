using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace PMS.Host.Models
{
    /// <summary>
    /// Describes task entity members
    /// </summary>
    [DataContract]
    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Task
    {
        /// <summary>
        /// Gets or sets the task code. THis is a primary key for entity.
        /// </summary>
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the parent task code reference.
        /// </summary>
        [DataMember(Name = "parent")]
        public Guid? ParentTask { get; set; }


        /// <summary>
        /// Gets or sets the project reference code.
        /// </summary>
        [DataMember(Name = "project")]
        [Required]
        public string ProjectReference { get; set; }

        /// <summary>
        /// Gets or sets the task name.
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the task start date.
        /// </summary>
        [DataMember(Name = "startDate")]
        [Required]
        public DateTimeOffset StartDate { get; set; }

        /// <summary>
        /// Gets or sets the task expiration date.
        /// </summary>
        [DataMember(Name = "finishDate")]
        public DateTimeOffset? FinishDate { get; set; }

        /// <summary>
        /// Gets or sets the task state.
        /// </summary>
        [DataMember(Name = "state")]
        [Required]
        public TaskState State { get; set; }
    }
}