﻿using System.Runtime.Serialization;

namespace PMS.Host.Models
{
    /// <summary>
    /// Error response contract
    /// </summary>
    [DataContract]
    public class ErrorResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorResponse"/> class.
        /// </summary>
        public ErrorResponse()
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorResponse"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public ErrorResponse(string message)
        { 
            Message = message;
        }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        [DataMember(Name = "message")]
        public string Message { get; set; }
    }
}