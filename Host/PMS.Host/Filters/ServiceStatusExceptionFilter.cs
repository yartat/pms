﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using PMS.Host.Models;
using System;
using System.Net;

namespace PMS.Host.Filters
{
    /// <summary>
    /// Describes service status exception filter method
    /// </summary>
    /// <seealso cref="ExceptionFilterAttribute" />
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ServiceStatusExceptionFilter : ExceptionFilterAttribute
    {
        private readonly ILogger<ServiceStatusExceptionFilter> _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceStatusExceptionFilter"/> class.
        /// </summary>
        /// <param name="logger">The logger instance.</param>
        public ServiceStatusExceptionFilter(ILogger<ServiceStatusExceptionFilter> logger)
        {
            _logger = logger;
        }

        /// <inheritdoc />
        public override void OnException(ExceptionContext context)
        {
            var processingException = context?.Exception;
            if (processingException == null)
                return;

            using (_logger.BeginScope(("Session", context.HttpContext?.TraceIdentifier)))
            {
                _logger.LogError(
                    message:
                    $"{processingException.GetType().Name}. Processing ended with error message: '{processingException.Message ?? string.Empty}'",
                    exception: processingException,
                    eventId: 0);
            }

            context.Result = new JsonResult(
                    new ServiceStatusResponse
                    {
                        Version = new Version(1, 0, 0, 0)
                    })
            {
                StatusCode = (int)HttpStatusCode.InternalServerError
            };
            context.Exception = null;
            context.ExceptionHandled = true;
        }
    }
}