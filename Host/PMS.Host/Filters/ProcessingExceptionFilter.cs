﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using PMS.Host.Models;
using System;
using System.Net;
using System.Security.Authentication;

namespace PMS.Host.Filters
{
    /// <summary>
    /// Default exception filter
    /// </summary>
    /// <seealso cref="ExceptionFilterAttribute" />
    public class ProcessingExceptionFilter : ExceptionFilterAttribute
    {
        private readonly ILogger<ProcessingExceptionFilter> _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessingExceptionFilter"/> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        public ProcessingExceptionFilter(ILogger<ProcessingExceptionFilter> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Called when [exception].
        /// </summary>
        /// <param name="context">The exception context.</param>
        public override void OnException(ExceptionContext context)
        {
            var exception = context?.Exception;
            if (exception == null)
                return;

            using (_logger.BeginScope(("Session", context.HttpContext?.TraceIdentifier)))
            {
                _logger.LogError(
                    message:
                    $"{exception.GetType().Name}. Processing ended with error message: '{exception.Message}'",
                    exception: exception,
                    eventId: 0);
            }

            ObjectResult Result(string m, HttpStatusCode c) => new ObjectResult(new ErrorResponse(m))
            {
                StatusCode = (int)c
            };

            switch (exception)
            {
                case UnauthorizedAccessException _:
                case AuthenticationException _:
                    {
                        context.Result = Result(exception.Message, HttpStatusCode.Unauthorized);
                        break;
                    }
                default:
                    {
                        context.Result = Result(exception.Message, HttpStatusCode.InternalServerError);
                        break;
                    }
            }

            context.Exception = null;
            context.ExceptionHandled = true;
        }
    }
}
