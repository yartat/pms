﻿using Microsoft.Extensions.DependencyInjection;
using PMS.Host.Filters;

namespace PMS.Host.Extensions
{
    /// <summary>
    /// Extensions for add service filters to container
    /// </summary>
    public static class FiltersExtensions
    {
        /// <summary>
        /// Adds the exception filters to a DI container.
        /// </summary>
        /// <param name="services">The service collection instance.</param>
        /// <returns>Returns the service collection instance.</returns>
        public static IServiceCollection AddExceptionFilters(this IServiceCollection services)
        {
            return services
                .AddScoped<ServiceStatusExceptionFilter>()
                .AddScoped<ProcessingExceptionFilter>();
        }
    }
}