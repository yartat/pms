﻿using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace PMS.Host.Extensions
{
    /// <summary>
    /// Describes service status helper methods
    /// </summary>
    public static class ServiceStatusControllerHelper
    {
        /// <summary>
        /// Returns information about service assembly version
        /// </summary>
        /// <returns>Returns service <see cref="Version"/>.</returns>
        public static Version GetAssemblyVersion()
        {
            // Assembly version info
            var fileName = Assembly.GetEntryAssembly().Location;
            if (string.IsNullOrEmpty(fileName))
                throw new NotSupportedException();

            var versionInfo = FileVersionInfo.GetVersionInfo(fileName);
            return new Version(
                versionInfo.ProductMajorPart,
                versionInfo.ProductMinorPart,
                versionInfo.ProductBuildPart,
                versionInfo.ProductPrivatePart);
        }

        /// <summary>
        /// Returns information about the machine on which the service was started
        /// </summary>
        /// <returns>Returns machine information as <see cref="JObject"/>.</returns>
        public static JObject GetMachineInfo()
        {
            // Machine info
            var process = Process.GetCurrentProcess();
            return JObject.FromObject(new
            {
                Platform = GetPlatformName(),
                Architecture = RuntimeInformation.OSArchitecture.ToString(),
                OsDescription = RuntimeInformation.OSDescription,
                MemoryUsage = process.WorkingSet64,
                ThreadsCount = process.Threads.Count,
                CpuCount = Environment.ProcessorCount,
                Environment.MachineName,
                LocalTime = DateTimeOffset.Now,
                TimeZones = string.Join(",", TimeZoneInfo.GetSystemTimeZones().Select(x => x.Id)),
            });
        }

        private static string GetPlatformName()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                return OSPlatform.Linux.ToString();
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                return OSPlatform.Windows.ToString();
            if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                return OSPlatform.OSX.ToString();
            return "Unknown";
        }
    }
}