﻿using AutoMapper;
using PMS.Host.Models;

namespace PMS.Host.Extensions
{
    /// <summary>
    /// Describes mapper methods
    /// </summary>
    public static class ModelExtensions
    {
        private static IMapper _mapper;

        static ModelExtensions()
        { 
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Pms.Dal.Entity.ProjectState, ProjectState>();
                cfg.CreateMap<Pms.Dal.Entity.Project, Project>();
                cfg.CreateMap<Pms.Dal.Entity.TaskState, TaskState>();
                cfg.CreateMap<Pms.Dal.Entity.Task, Task>();
            });

            _mapper = config.CreateMapper();
        }

        /// <summary>
        /// Converts to task model.
        /// </summary>
        /// <param name="task">The task entity.</param>
        /// <returns>Returns model task instance.</returns>
        public static Task ToModel(this Pms.Dal.Entity.Task task)
        { 
            if (task == null)
            { 
                return null;
            }

            return _mapper.Map<Task>(task);
        }

        /// <summary>
        /// Converts to project model.
        /// </summary>
        /// <param name="project">The project entity.</param>
        /// <returns>Returns model project instance.</returns>
        public static Project ToModel(this Pms.Dal.Entity.Project project)
        {
            if (project == null)
            {
                return null;
            }

            return _mapper.Map<Project>(project);
        }
    }
}
