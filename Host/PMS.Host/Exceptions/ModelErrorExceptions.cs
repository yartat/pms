﻿using System;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace PMS.Host.Exceptions
{
    /// <summary>
    /// Describes model error exception methods.
    /// </summary>
    public class ModelErrorExceptions : Exception
    {
        /// <summary>
        /// Gets the errors.
        /// </summary>
        /// <value>The errors.</value>
        public ModelStateDictionary Errors { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelErrorExceptions"/> class.
        /// </summary>
        /// <param name="errors">The errors.</param>
        public ModelErrorExceptions(ModelStateDictionary errors) => Errors = errors;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelErrorExceptions"/> class.
        /// </summary>
        /// <param name="errors">The errors.</param>
        /// <param name="innerException">The inner exception.</param>
        public ModelErrorExceptions(ModelStateDictionary errors, Exception innerException)
            : base(string.Empty, innerException) =>
            Errors = errors;
    }
}