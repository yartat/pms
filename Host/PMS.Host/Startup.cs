﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using PMS.Dal.Extensions;
using PMS.Host.Extensions;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;

namespace PMS.Host
{
    /// <summary>
    /// Startup application class
    /// </summary>
    public class Startup
    {
        private const string DbConnectionEnvironmentName = "DB_CONNECTION";
        private const string DbConnectionConnectionString = "Database";
        private const string ServiceApiVersion = "v1";
        private const string ServiceApiName = "PMS";

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup" /> class with host environment.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Gets the application configuration.
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Configures the services.
        /// </summary>
        /// <param name="services">The service collection instance.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services
                .AddMvc();

            services
                .AddCors()
                .AddRepository(Configuration.GetValue<string>(DbConnectionEnvironmentName) ?? Configuration.GetConnectionString(DbConnectionConnectionString))
                .AddExceptionFilters()
                .AddResponseCompression()
                .AddHttpContextAccessor()
                .AddSwaggerGen(options =>
                {
                    options.SwaggerDoc(
                        ServiceApiVersion,
                        new Info
                        {
                            Version = ServiceApiVersion,
                            Title = $"{ServiceApiName} {ServiceApiVersion}",
                            Description = $"{ServiceApiName} {ServiceApiVersion}"
                        });

                    options.IncludeXmlComments(GetXmlPath());
                });

            string GetXmlPath(string name = null) =>
                Path.Combine(PlatformServices.Default.Application.ApplicationBasePath,
                    string.IsNullOrEmpty(name)
                        ? $"{PlatformServices.Default.Application.ApplicationName}.xml"
                        : $"{name}.xml");
        }

        /// <summary>
        /// Configures the specified application.
        /// </summary>
        /// <param name="app">The application builder instance.</param>
        /// <param name="lifetime">The application lifetime instance.</param>
        public void Configure(
            IApplicationBuilder app,
            IApplicationLifetime lifetime)
        {
            // Configure middleware
            app
                .UseCors(builder => 
                    builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                )
                .UseResponseCompression()
                .UseMvc()
                .UseSwagger()
                .UseSwaggerUI(options =>
                {
                    // fix validation error
                    // see http://stackoverflow.com/questions/32188386/cant-read-from-file-issue-in-swagger-ui
                    options.EnableValidator(null);
                    options.SwaggerEndpoint("/swagger/v1/swagger.json", $"{ServiceApiName} {ServiceApiVersion}");
                });
        }
    }
}
