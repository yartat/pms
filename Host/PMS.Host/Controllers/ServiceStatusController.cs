﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using PMS.Host.Extensions;
using PMS.Host.Filters;
using PMS.Host.Models;
using System;
using System.Net;
using System.Text;

namespace PMS.Host.Controllers
{
    /// <summary>
    /// Describes service status controller methods
    /// </summary>
    [Route("")]
    [ServiceFilter(typeof(ServiceStatusExceptionFilter))]
    public class ServiceStatusController : Controller
    {
        private readonly Version _assemblyVersion;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceStatusController"/> class.
        /// </summary>
        public ServiceStatusController()
        {
            _assemblyVersion = ServiceStatusControllerHelper.GetAssemblyVersion();
        }

        /// GET /status
        /// <summary>
        /// Gets service status as current version in Prometheus format
        /// </summary>
        /// <response code="200">Retrieving service status completed successfully.</response>
        /// <response code="500">Internal service error.</response>
        [HttpGet("status")]
        [Produces("text/plain")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.InternalServerError)]
        public IActionResult Status()
        {
            var sb = new StringBuilder(1000)
                .AppendLine("# GET current build/version number")
                .AppendLine($"prometheus_build_info{{version=\"{_assemblyVersion}\"}}");
            return Ok(sb.ToString());
        }

        /// GET /info
        /// <summary>
        /// Gets service version, machine info, environment
        /// </summary>
        /// <param name="environment">The hosting environment provider instance.</param>
        /// <response code="200">Retrieving service information completed successfully.</response>
        /// <response code="500">Internal service error.</response>
        [HttpGet("info")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(object), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.InternalServerError)]
        public IActionResult Info(
            [FromServices] IHostingEnvironment environment)
        {
            return Ok(new ServiceStatusResponse
            {
                Version = _assemblyVersion,
                Environment = JObject.FromObject(new 
                {
                    environment.EnvironmentName,
                    environment.ContentRootPath,
                    environment.WebRootPath,
                    environment.ApplicationName,
                }),
                MachineInfo = ServiceStatusControllerHelper.GetMachineInfo()
            });
        }
    }
}