﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PMS.Dal;
using PMS.Host.Extensions;
using PMS.Host.Filters;
using PMS.Host.Models;
using System;
using System.Net;
using System.Threading.Tasks;

namespace PMS.Host.Controllers
{
    /// <summary>
    /// Health check controller
    /// </summary>
    [Route("api/v1/project")]
    [ServiceFilter(typeof(ProcessingExceptionFilter))]
    [Produces("application/json")]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    public class ProjectsController : Controller
    {
        private readonly IRepository _repository;
        private readonly ILogger<ProjectsController> _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectsController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="logger">The logger instance</param>
        /// <exception cref="ArgumentNullException">
        /// repository
        /// or
        /// logger
        /// </exception>
        public ProjectsController(
            IRepository repository,
            ILogger<ProjectsController> logger)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        /// GET /api/v1/project/{code}
        /// <summary>
        /// Gets project information by code
        /// </summary>
        /// <returns>Returns project information.</returns>
        /// <param name="code">The project unique code.</param>
        /// <response code="200">Project information was retrieved successfully.</response>
        /// <response code="404">Project was not found.</response>
        /// <response code="500">Internal service error.</response>
        [HttpGet("{code}")]
        [ProducesResponseType(typeof(Project), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Get(string code)
        {
            var project = await _repository.GetProjectAsync(code);
            return project != null ?
                Ok(project.ToModel()) :
                (IActionResult) NotFound();
        }

        /// POST /api/v1/project
        /// <summary>
        /// Creates a new project
        /// </summary>
        /// <returns>Returns project new location.</returns>
        /// <param name="project">The project information.</param>
        /// <response code="201">Project was created successfully.</response>
        /// <response code="500">Internal service error.</response>
        [HttpPost()]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.Created)]
        [ValidateModelState]
        public async Task<IActionResult> Create([FromBody] Project project)
        {
            new ModelError(_logger)
                .AddIfWrong("state", project.State != ProjectState.Planned, "State must be a planned in new project")
                .ThrowIfAny();

            using (var scope = _repository.BeginScope())
            { 
                var projectEntity = await _repository.AddProjectAsync(project.Code, (Pms.Dal.Entity.Project) null);
                projectEntity.Name = project.Name;
                projectEntity.FinishDate = project.FinishDate;
                projectEntity.StartDate = project.StartDate;
                projectEntity.State = Pms.Dal.Entity.ProjectState.Planned;

                await scope.CommitChangesAsync();
            }

            return Created($"/api/v1/project/{project.Code}", null);
        }

    }
}