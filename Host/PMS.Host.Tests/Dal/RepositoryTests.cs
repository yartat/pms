using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Pms.Dal.Exception;
using PMS.Dal;
using System;
using System.Threading.Tasks;
using Xunit;
using Project = Pms.Dal.Entity.Project;
using ProjectState = Pms.Dal.Entity.ProjectState;
using Task = System.Threading.Tasks.Task;

namespace PMS.Host.Tests.Dal
{
    public class RepositoryTests : IDisposable
    {
        private const string ProjectCode = "CODE";
        private readonly ServiceProvider _provider;
        private bool _disposed;

        public RepositoryTests()
        {
            var services = new ServiceCollection();

            services
                .AddEntityFrameworkSqlServer()
                .AddDbContext<PmsContext>(builder => builder.UseInMemoryDatabase("PMS", (b) => new InMemoryDbContextOptionsBuilder(new DbContextOptionsBuilder().ConfigureWarnings(wb => wb.Ignore(InMemoryEventId.TransactionIgnoredWarning)))))
                .AddScoped<IRepository, Repository>();

            _provider = services.BuildServiceProvider();
        }

        [Fact]
        public async Task AddProjectsWithSameCode()
        { 
            const string ProjectCode = "CODE";
            var repository = _provider.GetRequiredService<IRepository>();
            var projectOne = await repository.AddProjectAsync(ProjectCode, (string)null);
            await Assert.ThrowsAsync<UniqueConstraintException>(() => repository.AddProjectAsync(ProjectCode, (string)null));
            await Assert.ThrowsAsync<UniqueConstraintException>(() => repository.AddProjectAsync(ProjectCode, (Project)null));
            projectOne.Should().NotBeNull();
            projectOne.Code.Should().Be(ProjectCode);
        }

        [Fact]
        public async Task AddNestedProjects()
        {
            const string ProjectOneCode = "CODE";
            const string ProjectTwoCode = "code";
            var repository = _provider.GetRequiredService<IRepository>();
            var projectOne = await repository.AddProjectAsync(ProjectOneCode, (Project)null);
            var projectTwo = await repository.AddProjectAsync(ProjectTwoCode, projectOne);
            projectOne.Should().NotBeNull();
            projectOne.Code.Should().Be(ProjectOneCode);
            projectTwo.Should().NotBeNull();
            projectTwo.Code.Should().Be(ProjectTwoCode);
            projectTwo.Parent.Should().Be(projectOne);
            projectOne.SubProjects.Should().Contain(projectTwo);
        }

        [Fact]
        public async Task AddProjectNestedTasks()
        {
            var (project, taskOne, taskTwo, taskThree) = await Prepare();
            project.Should().NotBeNull();
            project.Code.Should().Be(ProjectCode);
            taskOne.Should().NotBeNull();
            taskOne.Project.Should().Be(project);
            taskTwo.Project.Should().Be(project);
            taskThree.Project.Should().Be(project);
            taskThree.Parent.Should().Be(taskTwo);
            taskTwo.SubTasks.Should().Contain(taskThree);
            project.SubProjects.Should().BeNullOrEmpty();
            project.Tasks.Should().BeEquivalentTo(new [] { taskOne, taskTwo, taskThree });
        }

        [Fact]
        public async Task GetProject()
        {
            var (_, taskOne, taskTwo, taskThree) = await Prepare();
            var repository = _provider.GetRequiredService<IRepository>();
            var resultProject = await repository.GetProjectAsync(ProjectCode);
            resultProject.Should().NotBeNull();
            resultProject.Code.Should().Be(ProjectCode);
            resultProject.SubProjects.Should().BeNullOrEmpty();
            resultProject.Tasks.Should().BeEquivalentTo(new[] { taskOne, taskTwo, taskThree });
        }

        [Fact]
        public async Task GetTask()
        {
            var (expectedProject, _, taskTwo, taskThree) = await Prepare();
            var repository = _provider.GetRequiredService<IRepository>();
            var resultTask = await repository.GetTaskAsync(taskTwo.Id);
            resultTask.Should().NotBeNull();
            resultTask.Id.Should().Be(taskTwo.Id);
            resultTask.Project.Should().Be(expectedProject);
            resultTask.SubTasks.Should().Contain(taskThree);
        }

        [Fact]
        public async Task RemoveProjectFailed()
        {
            var (project, _, _, _) = await Prepare();
            var repository = _provider.GetRequiredService<IRepository>();
            await Assert.ThrowsAsync<IntegrityConstraintException>(() => repository.RemoveProjectAsync(ProjectCode));
        }

        [Fact]
        public async Task RemoveProjectSuccess()
        {
            const string ProjectCodeSuccess = "Code";
            var repository = _provider.GetRequiredService<IRepository>();
            var project = await repository.AddProjectAsync(ProjectCodeSuccess, (Project)null);
            project.State = ProjectState.InProgress;
            await repository.RemoveProjectAsync(project);
        }

        [Fact]
        public async Task RemoveTaskFailed()
        {
            var (_, _, taskTwo, _) = await Prepare();
            var repository = _provider.GetRequiredService<IRepository>();
            await Assert.ThrowsAsync<IntegrityConstraintException>(() => repository.RemoveTaskAsync(taskTwo.Id));
        }

        [Fact]
        public async Task RemoveTaskSuccess()
        {
            var (_, taskOne, _, _) = await Prepare();
            var repository = _provider.GetRequiredService<IRepository>();
            await repository.RemoveTaskAsync(taskOne);
        }

        public void Dispose()
        {
            if (_disposed)
            { 
                return;
            }

            _provider?.Dispose();
            _disposed = true;
        }

        private async Task<(Project project, Pms.Dal.Entity.Task taskOne, Pms.Dal.Entity.Task taskTwo, Pms.Dal.Entity.Task taskThree)> Prepare()
        {
            var repository = _provider.GetRequiredService<IRepository>();
            var project = await repository.AddProjectAsync(ProjectCode, (Project)null);
            var taskOne = await repository.AddTaskAsync(project, (Guid?)null);
            var taskTwo = await repository.AddTaskAsync(project, (Pms.Dal.Entity.Task)null);
            var taskThree = await repository.AddTaskAsync(project, taskTwo);
            return (project, taskOne, taskTwo, taskThree);
        }
    }
}
