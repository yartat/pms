REM build
dotnet restore PMS.sln
dotnet publish -c Release /p:Version=%1

REM run unit tests and coverage
dotnet test ./Host/PMS.Host.Tests/PMS.Host.Tests.csproj --results-directory ./testresults --logger "trx;LogFileName=test_results.xml" /p:CollectCoverage=true /p:CoverletOutputFormat=cobertura /p:CoverletOutput=../../testresults/coverage.cobertura.xml
reportgenerator "-reports:./testresults/coverage.cobertura.xml" "-targetdir:./reports" "-reporttypes:HTMLInline;HTMLChart"
