﻿using Pms.Dal.Entity;
using Pms.Dal.Exception;
using System;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace PMS.Dal
{
    /// <summary>
    /// Describe repository methods
    /// </summary>
    public class Repository : IRepository
    {
        private bool _disposed;
        private PmsContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class with specified <paramref name="context"/>.
        /// </summary>
        /// <param name="context">The database context instance.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is null</exception>
        public Repository(PmsContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        /// <inheritdoc />
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <inheritdoc />
        public IRepositoryScope BeginScope()
        {
            return new RepositoryTransaction(_context);
        }

        /// <inheritdoc />
        public void EnsureCreated()
        {
            _context.Database.EnsureCreated();
        }

        /// <summary>
        /// Performs tasks associated with releasing unmanaged resources.
        /// </summary>
        /// <param name="dispose">The flag that indicates call from <see cref="Dispose"/> method</param>
        protected void Dispose(bool dispose)
        {
            if (_disposed)
            {
                return;
            }

            if (dispose)
            {
                _context?.Dispose();
            }

            _disposed = true;
        }

        /// <inheritdoc />
        public async Task<Project> AddProjectAsync(string code, string parent = null)
        {
            try
            { 
                return (await _context.Projects.AddAsync(new Project { Code = code, ParentProject = parent })).Entity;
            }
            catch (InvalidOperationException ex)
            {
                throw new UniqueConstraintException($"Add project entity with code '{code}' was failed.", ex);
            }
        }

        /// <inheritdoc />
        public async Task<Project> AddProjectAsync(string code, Project parent = null)
        {
            try
            { 
                return (await _context.Projects.AddAsync(new Project { Code = code, Parent = parent })).Entity;
            }
            catch (InvalidOperationException ex)
            {
                throw new UniqueConstraintException($"Add project entity with code '{code}' was failed.", ex);
            }
        }

        /// <inheritdoc />
        public async Task<Pms.Dal.Entity.Task> AddTaskAsync(Project project, Guid? parent = null)
        {
            try
            {
                return (await _context.Tasks.AddAsync(new Pms.Dal.Entity.Task { Project = project, ParentTask = parent })).Entity;
            }
            catch (InvalidOperationException ex)
            {
                throw new UniqueConstraintException($"Add task entity was failed.", ex);
            }
        }

        /// <inheritdoc />
        public async Task<Pms.Dal.Entity.Task> AddTaskAsync(Project project, Pms.Dal.Entity.Task parent = null)
        {
            try
            {
                return (await _context.Tasks.AddAsync(new Pms.Dal.Entity.Task { Project = project, Parent = parent })).Entity;
            }
            catch (InvalidOperationException ex)
            {
                throw new UniqueConstraintException($"Add task entity was failed.", ex);
            }
        }

        /// <inheritdoc />
        public Task<Project> GetProjectAsync(string code) => _context.Projects.FindAsync(code);

        /// <inheritdoc />
        public Task RemoveProjectAsync(string code)
        {
            try
            {
                _context.Projects.Remove(new Project { Code = code });
                return Task.CompletedTask;
            }
            catch (InvalidOperationException ex)
            { 
                throw new IntegrityConstraintException($"Remove project with code '{code}' was failed", ex);
            }
        }

        /// <inheritdoc />
        public Task RemoveProjectAsync(Project project)
        {
            try
            {
                if (project != null)
                { 
                    _context.Projects.Remove(project);
                }

                return Task.CompletedTask;
            }
            catch (InvalidOperationException ex)
            {
                throw new IntegrityConstraintException($"Remove project with code '{project.Code}' was failed", ex);
            }
        }

        /// <inheritdoc />
        public Task<Pms.Dal.Entity.Task> GetTaskAsync(Guid id) => _context.Tasks.FindAsync(id);

        /// <inheritdoc />
        public Task RemoveTaskAsync(Guid id)
        {
            try
            {
                _context.Tasks.Remove(new Pms.Dal.Entity.Task { Id = id });
                return Task.CompletedTask;
            }
            catch (InvalidOperationException ex)
            {
                throw new IntegrityConstraintException($"Remove task with id '{id}' was failed", ex);
            }
        }

        /// <inheritdoc />
        public Task RemoveTaskAsync(Pms.Dal.Entity.Task task)
        {
            try
            {
                if (task != null)
                { 
                    _context.Tasks.Remove(task);
                }

                return Task.CompletedTask;
            }
            catch (InvalidOperationException ex)
            {
                throw new IntegrityConstraintException($"Remove task with id '{task.Id}' was failed", ex);
            }
        }
    }
}