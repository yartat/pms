﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PMS.Dal.Migrations
{
    public partial class Fix_Finish_Dates_Null : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "finish_date",
                schema: "pms",
                table: "Tasks",
                nullable: true,
                oldClrType: typeof(DateTimeOffset));

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "finish_date",
                schema: "pms",
                table: "Projects",
                nullable: true,
                oldClrType: typeof(DateTimeOffset));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "finish_date",
                schema: "pms",
                table: "Tasks",
                nullable: false,
                oldClrType: typeof(DateTimeOffset),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "finish_date",
                schema: "pms",
                table: "Projects",
                nullable: false,
                oldClrType: typeof(DateTimeOffset),
                oldNullable: true);
        }
    }
}
