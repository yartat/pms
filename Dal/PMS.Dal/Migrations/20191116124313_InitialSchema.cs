﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PMS.Dal.Migrations
{
    public partial class InitialSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "pms");

            migrationBuilder.CreateTable(
                name: "Projects",
                schema: "pms",
                columns: table => new
                {
                    code = table.Column<string>(maxLength: 30, nullable: false),
                    parent = table.Column<string>(maxLength: 30, nullable: true),
                    name = table.Column<string>(maxLength: 50, nullable: false),
                    start_date = table.Column<DateTimeOffset>(nullable: false),
                    finish_date = table.Column<DateTimeOffset>(nullable: false),
                    state = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.code);
                    table.ForeignKey(
                        name: "FK_Projects_Projects_parent",
                        column: x => x.parent,
                        principalSchema: "pms",
                        principalTable: "Projects",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                schema: "pms",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    parent = table.Column<Guid>(nullable: true),
                    project = table.Column<string>(maxLength: 30, nullable: false),
                    name = table.Column<string>(maxLength: 50, nullable: false),
                    start_date = table.Column<DateTimeOffset>(nullable: false),
                    finish_date = table.Column<DateTimeOffset>(nullable: false),
                    state = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.id);
                    table.ForeignKey(
                        name: "FK_Tasks_Tasks_parent",
                        column: x => x.parent,
                        principalSchema: "pms",
                        principalTable: "Tasks",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tasks_Projects_project",
                        column: x => x.project,
                        principalSchema: "pms",
                        principalTable: "Projects",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Projects_parent",
                schema: "pms",
                table: "Projects",
                column: "parent");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_parent",
                schema: "pms",
                table: "Tasks",
                column: "parent");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_project",
                schema: "pms",
                table: "Tasks",
                column: "project");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks",
                schema: "pms");

            migrationBuilder.DropTable(
                name: "Projects",
                schema: "pms");
        }
    }
}
