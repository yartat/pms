﻿using System;
using System.Threading.Tasks;

namespace PMS.Dal
{
    /// <summary>
    /// Describes repository scope interface. For example, in RDBMS it is a transaction
    /// </summary>
    public interface IRepositoryScope : IDisposable
    {
        /// <summary>
        /// Commits the changes asynchronous.
        /// </summary>
        /// <returns></returns>
        Task CommitChangesAsync();
    }
}