﻿using Pms.Dal.Entity;
using System;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace PMS.Dal
{
    /// <summary>
    /// Describes repository access interface
    /// </summary>
    public interface IRepository : IDisposable
    {
        /// <summary>
        /// Begins repository scope.
        /// </summary>
        /// <returns>Returns repository scope with specified isolation level.</returns>
        IRepositoryScope BeginScope();

        /// <summary>
        /// Adds the project entity to database asynchronous.
        /// </summary>
        /// <param name="code">The project code as a primary key.</param>
        /// <param name="parent">The parent project reference code.</param>
        /// <returns>Returns <see cref="Project"/> instance.</returns>
        Task<Project> AddProjectAsync(string code, string parent = null);

        /// <summary>
        /// Adds the project entity to database asynchronous.
        /// </summary>
        /// <param name="code">The project code as a primary key.</param>
        /// <param name="parent">The parent project reference.</param>
        /// <returns>Returns <see cref="Project"/> instance.</returns>
        Task<Project> AddProjectAsync(string code, Project parent = null);

        /// <summary>
        /// Adds the task entity to database asynchronous.
        /// </summary>
        /// <param name="project">The parent project.</param>
        /// <param name="parent">The parent task reference code.</param>
        /// <returns>Returns <see cref="Pms.Dal.Entity.Task"/> instance.</returns>
        Task<Pms.Dal.Entity.Task> AddTaskAsync(Project project, Guid? parent = null);

        /// <summary>
        /// Adds the task entity to database asynchronous.
        /// </summary>
        /// <param name="project">The parent project.</param>
        /// <param name="parent">The parent task reference.</param>
        /// <returns>Returns <see cref="Pms.Dal.Entity.Task"/> instance.</returns>
        Task<Pms.Dal.Entity.Task> AddTaskAsync(Project project, Pms.Dal.Entity.Task parent = null);

        /// <summary>
        /// Gets the project entity from database asynchronous.
        /// </summary>
        /// <param name="code">The project code.</param>
        /// <returns>Returns <see cref="Project"/> instance in case it was in database; elsewhere returns <b>null</b>.</returns>
        Task<Project> GetProjectAsync(string code);

        /// <summary>
        /// Removes the project entity from database asynchronous.
        /// </summary>
        /// <param name="code">The project code.</param>
        Task RemoveProjectAsync(string code);

        /// <summary>
        /// Removes the project entity from database asynchronous.
        /// </summary>
        /// <param name="project">The project instance.</param>
        Task RemoveProjectAsync(Project project);

        /// <summary>
        /// Gets the task entity from database asynchronous.
        /// </summary>
        /// <param name="id">The task id.</param>
        /// <returns>Returns <see cref="ms.Dal.Entity.Task"/> instance in case it was in database; elsewhere returns <b>null</b>.</returns>
        Task<Pms.Dal.Entity.Task> GetTaskAsync(Guid id);

        /// <summary>
        /// Removes the task entity from database asynchronous.
        /// </summary>
        /// <param name="code">The task id.</param>
        Task RemoveTaskAsync(Guid id);

        /// <summary>
        /// Removes the task entity from database asynchronous.
        /// </summary>
        /// <param name="task">The task instance.</param>
        Task RemoveTaskAsync(Pms.Dal.Entity.Task task);

        /// <summary>
        ///  Ensures that the database for the context exists. If it exists, no action is taken. If it does not
        ///  exist then the database and all its schema are created and applied all migrations.
        /// </summary>
        void EnsureCreated();
    }
}
