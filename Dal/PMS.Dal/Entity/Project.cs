using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Pms.Dal.Entity
{
    /// <summary>
    /// Describes project entity members
    /// </summary>
    [DataContract]
    public class Project
    {
        /// <summary>
        /// Gets or sets the project code. THis is a primary key for entity.
        /// </summary>
        [DataMember(Name = "code")]
        [Key]
        [Required]
        [Column("code", Order = 1)]
        [MaxLength(30)]
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the parent project code reference.
        /// </summary>
        [DataMember(Name = "parent")]
        [Column("parent", Order = 2)]
        [MaxLength(30)]
        public string ParentProject { get; set; }

        /// <summary>
        /// Gets or sets the project name.
        /// </summary>
        [DataMember(Name = "name")]
        [Column("name", Order = 3)]
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the project start date.
        /// </summary>
        [DataMember(Name = "startDate")]
        [Column("start_date", Order = 4)]
        [Required]
        public DateTimeOffset StartDate { get; set; }

        /// <summary>
        /// Gets or sets the project expiration date.
        /// </summary>
        [DataMember(Name = "finishDate")]
        [Column("finish_date", Order = 5)]
        public DateTimeOffset? FinishDate { get; set; }

        /// <summary>
        /// Gets or sets the project state.
        /// </summary>
        [DataMember(Name = "state")]
        [Column("state", Order = 6)]
        [Required]
        public ProjectState State { get; set; }

        /// <summary>
        /// Gets or sets the parent project.
        /// </summary>
        public virtual Project Parent { get;set; }

        /// <summary>
        /// Gets or sets the collection of the sub-projects.
        /// </summary>
        public virtual ICollection<Project> SubProjects { get; set; }

        /// <summary>
        /// Gets or sets the owned task collection.
        /// </summary>
        public virtual ICollection<Task> Tasks { get; set; }
    }
}