using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Pms.Dal.Entity
{
    /// <summary>
    /// Describes task entity members
    /// </summary>
    [DataContract]
    public class Task
    {
        /// <summary>
        /// Gets or sets the task code. THis is a primary key for entity.
        /// </summary>
        [DataMember(Name = "id")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        [Column("id", Order = 1)]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the parent task code reference.
        /// </summary>
        [DataMember(Name = "parent")]
        [Column("parent", Order = 2)]
        public Guid? ParentTask { get; set; }


        /// <summary>
        /// Gets or sets the project reference code.
        /// </summary>
        [DataMember(Name = "project")]
        [Column("project", Order = 3)]
        [Required]
        [MaxLength(30)]
        public string ProjectReference { get; set; }

        /// <summary>
        /// Gets or sets the task name.
        /// </summary>
        [DataMember(Name = "name")]
        [Column("name", Order = 4)]
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the task start date.
        /// </summary>
        [DataMember(Name = "startDate")]
        [Column("start_date", Order = 5)]
        [Required]
        public DateTimeOffset StartDate { get; set; }

        /// <summary>
        /// Gets or sets the task expiration date.
        /// </summary>
        [DataMember(Name = "finishDate")]
        [Column("finish_date", Order = 6)]
        public DateTimeOffset? FinishDate { get; set; }

        /// <summary>
        /// Gets or sets the task state.
        /// </summary>
        [DataMember(Name = "state")]
        [Column("state", Order = 6)]
        [Required]
        public TaskState State { get; set; }

        /// <summary>
        /// Gets or sets the parent task.
        /// </summary>
        public virtual Task Parent { get; set; }

        /// <summary>
        /// Gets or sets the collection of the sub-tasks.
        /// </summary>
        public virtual ICollection<Task> SubTasks { get; set; }

        /// <summary>
        /// Gets or sets the project reference.
        /// </summary>
        public virtual Project Project { get; set; }
    }
}