﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace PMS.Dal
{
    /// <summary>
    /// EF tools use this factory to create an instance of DbContext. This is required to enable migrations.
    /// Implements this interface to enable design-time services for context types that do not have a public default constructor. 
    /// Design-time services will automatically discover implementations of this interface that are in the same assembly as the derived context.
    /// </summary>
    /// <seealso cref="IDesignTimeDbContextFactory{PmsContext}" />
    public class MigrationContextFactory : IDesignTimeDbContextFactory<PmsContext>
    {
        private const string DbConnectionEnvironmentName = "DB_CONNECTION";
        private const string DbConnectionConnectionString = "Database";

        /// <inheritdoc />
        public PmsContext CreateDbContext(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .AddEnvironmentVariables()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile("appsettings.Development.json", optional: true);

            var configuration = builder.Build();

            var services = new ServiceCollection();
            services
                .AddLogging()
                .AddEntityFrameworkSqlServer()
                .AddDbContext<PmsContext>(optionBuilder => 
                    optionBuilder.UseSqlServer(configuration.GetValue<string>(DbConnectionEnvironmentName) ?? 
                    configuration.GetConnectionString(DbConnectionConnectionString)));
            var provider = services.BuildServiceProvider();
            var log = provider.GetRequiredService<ILogger<MigrationContextFactory>>();
            log.LogInformation($"Try processing database {configuration.GetValue<string>(DbConnectionEnvironmentName) ?? configuration.GetConnectionString(DbConnectionConnectionString)}");

            return provider.GetService<PmsContext>();
        }
    }
}