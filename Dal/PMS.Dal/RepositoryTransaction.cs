﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Pms.Dal.Exception;
using System;
using System.Threading.Tasks;

namespace PMS.Dal
{
    /// <summary>
    /// Implements database transaction as <see cref="IRepositoryScope"/>.
    /// </summary>
    /// <seealso cref="IRepositoryScope" />
    public class RepositoryTransaction : IRepositoryScope
    {
        private DbContext _context;
        private IDbContextTransaction _transaction;
        private bool _disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryTransaction"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public RepositoryTransaction(DbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _transaction = context.Database.BeginTransaction();
        }

        /// <inheritdoc />
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <inheritdoc />
        public async Task CommitChangesAsync()
        {
            try
            {
                await _context.SaveChangesAsync().ConfigureAwait(false);
                _transaction?.Commit();
            }
            catch (InvalidOperationException exception) // Catch update exception in InMemory repository
            {
                throw new UniqueConstraintException(exception.Message, exception);
            }
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _transaction?.Dispose();
            }

            _disposed = true;
        }
    }
}