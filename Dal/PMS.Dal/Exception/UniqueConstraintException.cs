﻿using System;

namespace Pms.Dal.Exception
{
    /// <summary>
    /// Duplicate key in repository
    /// </summary>
    [Serializable]
    public class UniqueConstraintException : IntegrityConstraintException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UniqueConstraintException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public UniqueConstraintException(string message, System.Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UniqueConstraintException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public UniqueConstraintException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UniqueConstraintException"/> class.
        /// </summary>
        public UniqueConstraintException() : base("Duplicate keys")
        {
        }
    }
}