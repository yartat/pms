﻿using System;

namespace Pms.Dal.Exception
{
    /// <summary>
    /// Integrity constraint violation in repository
    /// </summary>
    [Serializable]
    public class IntegrityConstraintException : RepositoryException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IntegrityConstraintException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public IntegrityConstraintException(string message, System.Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IntegrityConstraintException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        protected IntegrityConstraintException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IntegrityConstraintException"/> class.
        /// </summary>
        public IntegrityConstraintException() : base("Integrity constraint violation")
        {
        }
    }
}