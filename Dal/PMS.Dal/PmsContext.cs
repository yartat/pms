﻿using Microsoft.EntityFrameworkCore;
using Pms.Dal.Entity;

namespace PMS.Dal
{
    /// <summary>
    /// Describes PMS database context
    /// </summary>
    public class PmsContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PmsContext"/> class.
        /// </summary>
        /// <param name="options">The options for this context.</param>
        public PmsContext(DbContextOptions options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets players entities
        /// </summary>
        public DbSet<Project> Projects { get; set; }

        /// <summary>
        /// Gets or sets player metadata entities
        /// </summary>
        public DbSet<Task> Tasks { get; set; }

        /// <inheritdoc />
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasDefaultSchema("pms");

            modelBuilder
                .Entity<Task>()
                .HasOne(x => x.Project)
                .WithMany(x => x.Tasks)
                .HasForeignKey(x => x.ProjectReference)
                .IsRequired();

            modelBuilder
                .Entity<Task>()
                .HasMany(x => x.SubTasks)
                .WithOne(x => x.Parent)
                .HasForeignKey(x => x.ParentTask)
                .IsRequired(false);

            modelBuilder
                .Entity<Project>()
                .HasMany(x => x.SubProjects)
                .WithOne(x => x.Parent)
                .HasForeignKey(x => x.ParentProject)
                .IsRequired(false);

            base.OnModelCreating(modelBuilder);
        }
    }
}