﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace PMS.Dal.Extensions
{
    /// <summary>
    /// A service for adding repositories to a DI container
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Adds the repository to a DI container.
        /// </summary>
        /// <param name="services">The service collection instance.</param>
        /// <param name="connectionString">The connection string.</param>
        /// <returns>Returns service collection instance.</returns>
        public static IServiceCollection AddRepository(this IServiceCollection services, string connectionString)
        {
            return services
                    .AddEntityFrameworkSqlServer()
                    .AddDbContext<PmsContext>(builder => builder.UseSqlServer(connectionString))
                    .AddScoped<IRepository, Repository>();
        }
    }
}